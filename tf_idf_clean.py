"""
MASSE Océane & DAUDRÉ--TREUIL Prunelle

Dans ce fichier vous trouverez tout le nécéssaire à la réalisation de la méthode TF-IDF.
Le contenu sera abordé de manière plus ou moins vulgarisée lors de l'oral ainsi que dans
le rapport.
"""

##################### IMPORTS

from clean_comments import remove_comments

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer

##################### SCRIPT

# Choisissez le csv "XXXX_comments.csv"
csv_file = "presidential_debate_comments.csv"

# Supprime les lignes où il y a des commentaires des moins de 2 mots où ceux supprimés
clean_csv_file = remove_comments(csv_file)

# Créer l'objet, dont la liste de stop word est celle prédéfnie (en anglais)
# avec max_df et min_df (=1) les stops words sont ignorés
# avec norm=None le calcul de TF se fait par frequence brute
vectorizer = TfidfVectorizer(max_df=1.0, min_df=1, stop_words='english',norm = None)

# Calcul du TF-IDF pour chaque mot dans chaque document
# Il s'agit d'une matrice clairsemée, forme normale de dimension (nbr de commentaires, taille du vocabulaire)
X = vectorizer.fit_transform(clean_csv_file["comments"])

#_______________ Data intermediaires

# Liste du vocabulaire de tous les commentaires
X_vocab = vectorizer.get_feature_names()

# Occurence de chaque mot dans chaque commentaire
# matrice claisemée, forme normale de taille (nombre de commentaires, taille du voc)
c = CountVectorizer()
word_count = c.fit_transform(clean_csv_file["comments"])

# Liste score IDF pour chaque mot du vocabulaire
X_idf = vectorizer.idf_


############################### MAIN
# Espace pour tester sans gêner l'importation

if __name__ == "__main__":
    print(X)