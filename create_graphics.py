"""
Ce fichier permet la création des graphiques barplot des résultats du clustering pour les 3 subreddit.

MASSE Océane & DAUDRÉ--TREUIL Prunelle
"""


# libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 

#######


# set width of bar
barWidth = 0.25
 
# set height of bar

#####Presidential debate
"""
bars1 = [0.634280128618148,0.32747547874586636,0.3428977560321879,0.19714275073962753,0.1911112511422844,0.18894133854785627,0.18256522088329238,0.1737969334753556,0.3374171761486627,0.0000000000000000,0.0000000000000000,0.0000000000000000,0.0000000000000000]
bars2 = [3.154983734113851,2.7415988971852236,1.476216044778072,0.0000000000000000,0.0000000000000000,0.0000000000000000,0.7466669535394006,0.6995825988858565,1.3785662765884514,1.4228547189933909,1.3264909021329077,0.0000000000000000,0.0000000000000000]
bars3 = [4.543392671900255,8.63326723431811,2.996718570899486,0.0000000000000000,0.0000000000000000,0.0000000000000000,0.0000000000000000,0.0000000000000000,2.2715012511968813,4.6420635207159355,0.0000000000000000,2.3864414311256845,2.2965202012264503]
"""

##### Trump
"""
bars1 = [0.5319062187849741,0,1.30588101521695577,0,0.24049188141805652,0.3200947988597864,0.2250999606849838,0.3952433628236569,0.24021175695127298,0,0,0]
bars2 = [2.721729040363905,1.4796300617337932,0,0,2.183289041109023,0,0,0,0,1.2746065067045187,1.217590389092973,1.2073033858543873]
"""

##### Biden
bars1 = [0.326114717620843,0.26341686362756683,0.3048834404759457,0.21173238015261334,0.17568834159906505,0.13254968737164327,0,0,0,0,0,0,0]
bars2 = [0,0,1.2711895136610392,1.3833182169970735,0,0,0,1.694215876961197,1.6258570538053494,1.2813799420858727,1.2403454606454754,0.8859610433181965,0.8326865854567556]

# Set position of bar on X axis
r1 = np.arange(len(bars1))
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
 
# Make the plot

##### Presidential debate
"""
plt.bar(r1, bars1, color='#7f6d5f', width=barWidth, edgecolor='white', label='Cluster 1')
plt.bar(r2, bars2, color='#557f2d', width=barWidth, edgecolor='white', label='Cluster 2')
plt.bar(r3, bars3, color='#2d7f5e', width=barWidth, edgecolor='white', label='Cluster 3')
"""

##### Trump et Biden
plt.bar(r1, bars1, color='#7f6d5f', width=barWidth, edgecolor='white', label='Cluster 1')
plt.bar(r2, bars2, color='#557f2d', width=barWidth, edgecolor='white', label='Cluster 2')



# Add xticks on the middle of the group bars
plt.xlabel('Words', fontweight='bold')

### Presidential Debate
"""
plt.xticks([r + barWidth for r in range(len(bars1))], ["trump","biden","debate","time","white","tax","think","president","like","people","just","fucking","shit"], rotation=90)
"""

### Trump
""" 
plt.xticks([r + barWidth for r in range(len(bars1))], ["trump","white","tax","think","president","like","people","just","covid","mask","twitter","house"], rotation=90)
"""

## Biden
plt.xticks([r + barWidth for r in range(len(bars1))], ["mask","like","trump","biden","melania","bolsonaro","people","debate","time","mic","candidate","minutes","shut"], rotation=90)


# Create legend & Show graphic
plt.legend()
plt.show()
