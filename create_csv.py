"""
MASSE Océane
DAUDRÉ--TREUIL Prunelle

This create_csv.py file is used to create an other csv with all the comments, link by the ID to his original post.
"""

###### IMPORTS

import pandas as pd 
import typing
import csv

####### functions

def clean(string):
    return eval(string) # the string parsed and evaluated as a python expression

def create_csv(csv_name, csv_out_name, separator = ","):
    df = pd.read_csv(csv_name, sep = separator)
    df["ID"] = [i for i in range(0,df.shape[0])] # create an explicite ID column that will be usefull for the final csv file
    df_ID_comments = df[["ID", "comments"]] # make csv with essential columns 
    list_com = []
    for i, comments in enumerate(df_ID_comments["comments"]):
        for com in clean(comments): # we need to clean because '["string example]"]' is not interpreted as a list
            list_com.append([i, com]) 
    columns = ["ID", "comments"]
    with open(csv_out_name, "w", newline = '') as f: #create the csv
        write = csv.writer(f)
        write.writerow(columns)
        write.writerows(list_com)



###### SCRIPT

create_csv("keyword_biden.csv", "biden_comments.csv" )
create_csv("keyword_presidential_debate.csv", "presidential_debate_comments.csv")
create_csv("keyword_trump.csv", "trump_comments.csv")
