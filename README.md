# Débat Présidentiel américain sur Reddit - README

Projet réalisé dans le cadre du cours de traitement de données en L2 MIASHS à l'Université de Lille.

L'archive est organisée de la manière suivante :

1. Création de la base de données
    + Le dossier redQ (https://gitlab.com/Aethor) contient le nécéssaire pour effectuer les requêtes et créer les fichiers csv, les 3 mots clefs utilisés sont : "Presidential debate", "Trump" et "Biden"
    + Le fichier create_csv.py prend les fichiers issus du wrapper et créer d'autres fichiers csv avec l'ID du post d'origine et les commentaires

2. Base de données
    + Les fichiers originaux issus du wrapper selon les 3 mots clefs : keyword_XXXX.csv , ce ne sont pas ceux utilisés concrètement.
    + La base de donnée utilisée ( qui contient uniquement les commentaires et le lien à leur posts d'origine) : XXXX_comments.csv, crée par create_csv.py

3. Analyse de sentiments
    + Il s'agit du fichier SAwithVader2.py. Il est nécéssaire d'installer vaderSentiment pour l'utiliser, avec pip : ```pip3 install vaderSentiment```, ci besoin vous trouverez la documentation ici https://pypi.org/project/vaderSentiment/

4. Analyse de sujets de discussions
    + Vous trouverez la partie relative au TF-IDF dans le fichier tf_idf_clean.py, il est nécéssaire d'installer scikit-learn selon la documentation suivante https://scikit-learn.org/stable/install.html 
    + En ce qui concerne la création et l'analyse des sujets (cluster), il s'agit du fichier clustering.py

5. Résultats analyse sujets
    * Le fichier create_graphics.py permet l'analyse des résultats issus de clustering.py
    * Le json clusters_presidential_debate_comments.json est un exemple des clusters formés avec le fichier clustering.py pour le csv presidential_debate_comments.csv

6. Rapport écrit sous format pdf

Nous vous souhaitons bonne lecture.
MASSE Océane & DAUDRÉ--TREUIL Prunelle
